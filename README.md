# To install: #

- Download the tar.gz file
- Open R, and setwd() to the location of the tar file
- Run the following:

### Install dependencies: ###
install.packages(c("tau","SnowballC"))

### Install this package: ###
install.packages("wordCountWrapper_1.0.tar.gz",repos=NULL,type="source")


# To start using #

### Load the package ###
library(wordCountWrapper)

### List functions available in wordCountWrapper ###
lsf.str("package:wordCountWrapper")


# Available functions : #

### To Input Files: ###
Each returns a nested list with one character vector per sample
contains: full text, separated words, and modifications of the words

- readMultFiles - reads from multiple files
- readSingFile - reads samples from a single file, separated by a record separator (e.g. '>')
- convertTextInput - converts a character vector to the format needed for the counting operations

### To count token presence ###
Each returns a list with two matrices: one of counts and one of frequencies of token usage

- countWords - counts the presence of words (can count word roots)
- countNGrams - counts the presence of ngrams

### To get more information on the use of a token ###
- getContext - returns the words before and after every occurunce of your target(s)


# Miscellaneous info and downstream #
The table outputs from the counting functions are suited to be used in many other analysis and visualizaton tools.

For more usage information, or to report bugs and enhancement requests, please contact the authors.
This package was very recently converted from in-house scripts, so some idiosyncrasies may persist. 
