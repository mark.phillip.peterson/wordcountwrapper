readMultFiles <-
function(
  fileDir, # Directory to be read
  patternToRead = "*.txt$", ## Which files should be read in; uses regexp; set to "*" to read all
  patternToName = patternToRead, ##   Character vector of patterns to remove from the file name to name the samples
  changeNums = TRUE, ## Should all digits be changed to 7
  wordsToReplace = NULL ## character vector of required replacements of the form "SEARCH::REPLACEMENT"; uses regexp 
  ){
  
  if(!is.null(wordsToReplace)){
    replacers <- list()
    for(k in 1:length(wordsToReplace)){
      replacers[[k]] <- unlist(strsplit(wordsToReplace[k],split="::",fixed=TRUE))
    }  
  }
  
  
  
  ## Add slash to fileDir, if needed:
  if(length(grep("/$",fileDir))==0){
    fileDir <- paste(fileDir,"/",sep="")
  }
  
  ## Grab all ".txt" files from that directory:
  filesToAnal <- list.files(fileDir,pattern=patternToRead)
  
  ## Remove the patterns from the sample names
  sampName <- filesToAnal
  for(k in 1:length(patternToName)){
    sampName <- gsub(patternToName[k],"",sampName)
  }
  
  fullTextFiles <- tempDoc <- splitWords <- list()
  ## Run a loop through all files in the fileDir
  for(k in 1:length(filesToAnal)){
    ## Set this sample name
    thisSamp <- sampName[k]
    
    ## Read in the text file
    fullTextFiles[[thisSamp]] <- paste(scan(paste(fileDir,filesToAnal[k],sep=""), character(0),sep="\n", quiet = TRUE),collapse="\n\n")
    ## removing paste would return this as separate pp's
    
    ## Make the separate words:
    splitWords[[thisSamp]] <- scan(paste(fileDir,filesToAnal[k],sep=""), character(0), quiet = TRUE)
    
    ## This would be a nice way to do this, but it doesn't allow handling of non-unicode characters
#     tempSplit <- unlist(strsplit(fullTextFiles[[thisSamp]],"[[:space:]]") )
#     splitWords[[thisSamp]] <- tempSplit[tempSplit != ""]
    
    ## Remove the punctuation and non-english characters
    ## Also, make it all lower case
    tempText <- tolower(gsub("[^[:alnum:]$]","",splitWords[[thisSamp]])) ## removes everything but alphanumeric; "[[:punct:]]" removes punctuation, but leaves non-english characters
    
    ## Change all numbers to 7, if desired
    if(changeNums) {
      tempText <- gsub("[[:digit:]]","7",tempText)
    }
    
    ## Replace specific words
    if(!is.null(wordsToReplace)){
      for(j in 1:length(replacers)){
        tempText[grep(replacers[[j]][1],tempText)] <- replacers[[j]][2]
      }
    }
    
    
    tempDoc[[thisSamp]] <- tempText
    
  }
  
  out <- list(fullTextFiles = fullTextFiles, splitWords = splitWords, modifiedFiles = tempDoc)
  return(out)  
}
